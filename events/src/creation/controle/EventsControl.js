import store from "../../store.js";
import { NEW_EVENT_CREATED } from "../entity/EventReducer.js";

const createEvent = payload => {
    store.dispatch({
        type: NEW_EVENT_CREATED,
        //payload: payload
        payload
    })
}

export { createEvent }