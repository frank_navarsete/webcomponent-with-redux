import { createStore } from './lib/redux.js';
import eventsReducer from "./creation/entity/EventReducer.js";

const deepCopy = input => JSON.parse(JSON.stringify(input));

const copingEvent = (state, action) => {
    return deepCopy(eventsReducer(state, action));
}

const store = createStore(copingEvent,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

export default store;