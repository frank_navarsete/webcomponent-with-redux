import { html } from '../../lib/lit-html.js';
import AirElement from '../../AirElement.js';

class EventsOverview extends AirElement {

    view() {
        const eventList = this.state.events;
        console.log("UPDATE overview:", this.state);
        return html`
            <ul>
                ${eventList.map(({eventname, description}) => html`
                    <li>${eventname} ===> ${description}</li>
                `)}
            </ul>
        `;
    }
}

customElements.define('a-events-overview', EventsOverview);